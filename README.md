# big-data-tutorial

big data practice with MySQL, PySpark & AWS-EMR, Python, Docker, and S3

# Get it Rippin'
```bash
$ sudo chmod 777 services/csv_loader/entrypoint.sh
$ docker-compose up -d --build
```
