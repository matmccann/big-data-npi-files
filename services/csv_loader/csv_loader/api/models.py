# services/csv_loader/csv_loader/api/models.py


from csv_loader import db


class Data(db.Model):
    __tablename__ = 'csv_loader'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    entity_type_code = db.Column(db.Integer, nullable=False)
    replacement_npi = db.Column(db.Integer, nullable=False)
    ein = db.Column(db.String(128), default=True, nullable=False)
    provider_organization_name = db.Column(db.String(128), nullable=False)

    def __init__(self, npi, entity_type_code, replacement_npi,
                 ein, provider_organization_name):
        self.npi = npi
        self.entity_type_code = entity_type_code
        self.replacement_npi = replacement_npi
        self.ein = ein
        self.provider_organization_name = provider_organization_name

    def to_json(self):
        return {
            'npi': self.npi,
            'entity_type_code': self.entity_type_code,
            'replacement_npi': self.replacement_npi,
            'ein': self.ein,
            'provider_organization_name': self.provider_organization_name
        }
