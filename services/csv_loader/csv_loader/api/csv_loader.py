# services/csv_loader/csv_loader/api/csv_loader.py


from flask import Blueprint
from flask_restful import Resource, Api


users_blueprint = Blueprint('csv_loader', __name__)
api = Api(users_blueprint)


class DataStore(Resource):
    """ List Data in S3 bucket"""
    print('Data store is here')


class DownloadDataToS3(Resource):
    """ List Data in S3 bucket"""
    print('Input: URL; Output: folder name to store contents in S3 bucket')


class S3ToDatabase(Resource):
    """ List Data in S3 bucket"""
    print('Input: S3 bucket folder; Output: Database location')


api.add_resource(DataStore, '/csv_loader/data-store')
api.add_resource(DownloadDataToS3, '/csv_loader/download-to-s3')
api.add_resource(S3ToDatabase, '/csv_loader/s3-to-database')
