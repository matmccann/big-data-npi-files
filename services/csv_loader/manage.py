#!/usr/bin/env python
# services/csv_loader/manage.py


import sys
import unittest

from flask.cli import FlaskGroup

from csv_loader import create_app, db
from csv_loader.api.models import Data


app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    """Seeds the database."""

    db.session.add(Data(npi=1679576722, entity_type_code=1, replacement_npi="NaN",
                        ein="NaN", provider_organization_name="NaN"))
    db.session.commit()


@cli.command()
def test():
    """Runs the tests without code coverage"""
    tests = unittest.TestLoader().discover('csv_loader/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    sys.exit(result)


if __name__ == '__main__':
    cli()
