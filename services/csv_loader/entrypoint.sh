#!/bin/sh

echo "Waiting for MariaDB..."

while ! nc -z database 3306; do
  sleep 0.1
done

echo "MariaDB started"

python manage.py run -h 0.0.0.0